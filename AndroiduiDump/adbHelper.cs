﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using Utilities;

namespace AndroiduiDump
{
    class adbHelper
    {
        private StringBuilder _output = null;
        private Char _trim = '\r';
        private Boolean _quit = false;
        private string mFileName;

        public adbHelper(string sFileName)
        {
            mFileName = sFileName;
            if (string.IsNullOrEmpty(mFileName) || !System.IO.File.Exists(mFileName))
            {
                mFileName = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), "adb.exe");
            }

        }

        public static string getDeviceProp(string adbWorkFolder, string propName, string deviceId)
        {

            string ret = string.Empty;
            if (!string.IsNullOrEmpty(propName) && !string.IsNullOrEmpty(deviceId))
            {
                int n = 0;
                string adbExe = System.IO.Path.Combine(adbWorkFolder, "adb.exe");
                adbHelper a = new adbHelper(adbExe);
                string[] s = a.runAdb(String.Format("-s {0} shell getprop {1}", deviceId, propName), out n);
                if (s.Length > 0)
                {
                    ret = s[0];
                }
            }
            return ret;
        }

        public string getAdbOutput()
        {
            return _output.ToString();
        }

        public int runExe(string exe, string param,string sworkdir, int timeout = 10 * 10000)
        {
            int ret = 0;
            Logger.GetInstance().LogIt(string.Format("runExe: ++ {0}", param));
            _output = new StringBuilder();
            mFileName = exe;
            // 2. run adb
            if (System.IO.File.Exists(mFileName))
            {
                Process p = new Process();
                p.StartInfo.FileName = mFileName;
                p.StartInfo.Arguments = param;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.WorkingDirectory = sworkdir;
                //p.StartInfo.RedirectStandardInput = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.OutputDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);
                p.Exited += new EventHandler(p_Exited);
                p.EnableRaisingEvents = true;
                _quit = false;
                p.Start();
                p.BeginOutputReadLine();
                DateTime start = DateTime.Now;
                while (!_quit)
                {
                    DateTime now = DateTime.Now;
                    TimeSpan ts = now - start;
                    if (ts.TotalMilliseconds > timeout)
                    {
                        // timeout
                        Console.WriteLine("kill " + mFileName);
                        p.Kill();
                        _quit = true;
                    }
                    System.Threading.Thread.Sleep(1000);
                }
                if (!p.HasExited)
                    p.WaitForExit();
                ret = p.ExitCode;
            }

            Logger.GetInstance().LogIt(string.Format("runExe: ret={0}", ret));
            Logger.GetInstance().LogIt(_output.ToString());
            return ret;
        }

        public int runAdb(string adbParameters, Char trim = '\r', int timeout = 10*1000)
        {
            int ret = 0;
            Logger.GetInstance().LogIt(string.Format("runAdb: ++ {0}",adbParameters));
            _output = new StringBuilder();
            _trim = trim;


            // 2. run adb
            if (System.IO.File.Exists(mFileName))
            {
                Process p = new Process();
                p.StartInfo.FileName = mFileName;
                p.StartInfo.Arguments = adbParameters;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                //p.StartInfo.RedirectStandardInput = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.OutputDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);
                p.Exited += new EventHandler(p_Exited);
                p.EnableRaisingEvents = true;
                _quit = false;
                p.Start();
                p.BeginOutputReadLine();
                DateTime start = DateTime.Now;
                while (!_quit)
                {
                    DateTime now = DateTime.Now;
                    TimeSpan ts = now - start;
                    if (ts.TotalMilliseconds > timeout)
                    {
                        // timeout
                        Console.WriteLine("kill " + mFileName);
                        try
                        {
                            p.Kill();
                        }
                        catch (Exception)
                        {
                        	
                        }
                        _quit = true;
                    }
                    System.Threading.Thread.Sleep(1000);
                }
                if (!p.HasExited)
                    p.WaitForExit();
                ret = p.ExitCode;
            }

            Logger.GetInstance().LogIt(string.Format("runAdb: ret={0}", ret));
            Logger.GetInstance().LogIt(_output.ToString());
            return ret;
        }

        public int runAdb(string ssid, string[] runList, int timeout = 2*1000)
        {
            Logger.GetInstance().LogIt(string.Format("runAdb: ++ {0}", ssid));
            int ret = 0;
            _output = new StringBuilder();
            if (System.IO.File.Exists(mFileName))
            {
                Process p = new Process();
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = string.Format("/C \"{0}\" -s {1} shell", mFileName, ssid);
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardInput = true;
                //p.OutputDataReceived += new DataReceivedEventHandler(p_OutputDataReceived);
                p.Exited += new EventHandler(p_Exited);
                p.Start();
                p.StandardInput.AutoFlush = true;
                StreamWriter myStreamWriter = p.StandardInput;
                foreach (string s in runList)
                {
                    if (myStreamWriter.BaseStream.CanWrite)
                    {
                        myStreamWriter.WriteLine(s);
                        myStreamWriter.Flush();
                        Console.WriteLine(s);
                    }
                }
                if (myStreamWriter.BaseStream.CanWrite)
                {
                    myStreamWriter.WriteLine("exit");
                    myStreamWriter.Flush();
                    Console.WriteLine("exit");
                }
                p.StandardInput.Close();
                //p.BeginOutputReadLine();

                DateTime start = DateTime.Now;
                DateTime current = start;
                _quit = false;
                System.Threading.Thread.Sleep(100);
                if (!p.StandardOutput.EndOfStream)
                {
                    Console.WriteLine(p.StandardOutput.ReadToEnd());
                }
                while (!_quit)
                {
                    current = DateTime.Now;
                    TimeSpan t = current - start;
                    if (t.TotalMilliseconds > timeout)
                    {
                        if(!p.HasExited)
                            p.Kill();
                        _quit = true;
                        break;
                    }
                    System.Threading.Thread.Sleep(1000);
                }
                if (!p.HasExited)
                    p.WaitForExit();
                ret = p.ExitCode;
            }
            Logger.GetInstance().LogIt(_output.ToString());
            return ret;

        }

        public string[] runAdb(string adbParameters,out int exitCode, int timeout = 10*1000)
        {
            Logger.GetInstance().LogIt(string.Format("runAdb: ++ {0}", adbParameters));
            List<string> ret = new List<string>();
            exitCode = 0;
            if (System.IO.File.Exists(mFileName))
            {
                Process p = new Process();
                p.StartInfo.FileName = mFileName;
                p.StartInfo.Arguments = adbParameters;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                DateTime start = DateTime.Now;
                DateTime current = DateTime.Now;
                while (true)
                {
                    string s = p.StandardOutput.ReadLine();
                    if (s != null)
                    {
                        start = current;
                        ret.Add(s);
                    }
                    else
                        break;
                    current = DateTime.Now;
                    TimeSpan t = current - start;
                    if (t.TotalMilliseconds > timeout)
                    {
                        Console.WriteLine("kill " + mFileName);
                        p.Kill();
                        break;
                    }
                }
                if (!p.HasExited)
                    p.WaitForExit();
                exitCode = p.ExitCode;
            }
            return ret.ToArray();
        }

        void p_Exited(object sender, EventArgs e)
        {
            _quit = true;
        }

        void p_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                if (_output != null)
                {
                    _output.Append(e.Data);
                    if (_trim != 0)
                        _output.Append(_trim);
                }
            }
        }
    }
}
