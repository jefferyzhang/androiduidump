﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AndroiduiDump.Tree
{
    public class BasicTreeNode
    {

        public delegate void onFoundNode(BasicTreeNode paramBasicTreeNode);

          private static BasicTreeNode[] CHILDREN_TEMPLATE = new BasicTreeNode[0];
          protected BasicTreeNode mParent;
          protected List<BasicTreeNode> mChildren;
          public int x;
          public int y;
          public int width;
          public int height;
          protected Boolean mHasBounds;

          public BasicTreeNode()
          {
            this.mChildren = new List<BasicTreeNode>();
            this.mHasBounds = false;
          }
          public void addChild(BasicTreeNode child) {
            if (child == null) {
                throw new ArgumentNullException("Cannot add null child");
            }
            if (this.mChildren.Contains(child)) {
                throw new ArgumentNullException("node already a child");
            }
            this.mChildren.Add(child);
            child.mParent = this;
          }

          public List<BasicTreeNode> getChildrenList() {
            return this.mChildren;
          }

          public BasicTreeNode[] getChildren() {
              CHILDREN_TEMPLATE = (BasicTreeNode[])this.mChildren.ToArray();
              return CHILDREN_TEMPLATE;//.toArray(CHILDREN_TEMPLATE);
          }

          public BasicTreeNode getParent() {
            return this.mParent;
          }

          public Boolean hasChild() {
            return this.mChildren.Count() != 0;
          }

          public int getChildCount() {
              return this.mChildren.Count();
          }

          public void clearAllChildren() {
            foreach (BasicTreeNode child in this.mChildren) {
              child.clearAllChildren();
            }
            this.mChildren.Clear();
          }

          public Boolean findLeafMostNodesAtPoint(int px, int py, IFindNodeListener listener)
          {
            Boolean foundInChild = false;
            foreach (BasicTreeNode node in this.mChildren) {
              foundInChild |= node.findLeafMostNodesAtPoint(px, py, listener);
            }

            if (foundInChild) return true;

            if (this.mHasBounds) {
              if ((this.x <= px) && (px <= this.x + this.width) && (this.y <= py) && (py <= this.y + this.height)) {
                listener.onFoundNode(this);
                return true;
              }
              return false;
            }

            return false;
          }

          public Object[] getAttributesArray()
          {
            return null;
          }

          public interface IFindNodeListener
          {
             void onFoundNode(BasicTreeNode paramBasicTreeNode);
          }
    }
}
