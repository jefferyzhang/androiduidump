﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Xml;

namespace AndroiduiDump.Tree
{
    public class UiHierarchyXmlLoader
    {
        private BasicTreeNode mRootNode;
        private List<Rectangle> mNafNodes;

        private BasicTreeNode mParentNode;
        private BasicTreeNode mWorkingNode;

        private string findAttrValue(List<AttributePair> attributes, string strkey, Boolean ignoreCase)
        {
            string s = string.Empty;

            for (int i = 0; i < attributes.Count(); i++)
            {
                if (string.Compare(strkey, attributes[i].key, ignoreCase) == 0)
                {
                    s = attributes[i].value;
                    break;
                }
            }


            return s;
        }

        public void startElement(String uri, String localName, String qName, List<AttributePair> attributes) 
        {
            Boolean nodeCreated = false;

            this.mParentNode = this.mWorkingNode;
            if (string.Compare("hierarchy",qName,true)==0) 
            {
              int rotation = 0;
              for (int i = 0; i < attributes.Count(); i++) {
                if (string.Compare("rotation", attributes[i].key, true) == 0)
                  try {
                    rotation = Convert.ToInt32(attributes[i].value);
                  }
                  catch (FormatException)
                  {
                  }
              }
              this.mWorkingNode = new RootWindowNode(findAttrValue(attributes, "windowName", true), rotation);
              nodeCreated = true;
            } else if (string.Compare("node", qName, true) == 0) 
            {
              UiNode tmpNode = new UiNode();
              for (int i = 0; i < attributes.Count(); i++) 
              {
                tmpNode.addAtrribute(attributes[i].key, attributes[i].value);
              }
              this.mWorkingNode = tmpNode;
              nodeCreated = true;

              String naf = tmpNode.getAttribute("NAF");
              if (string.Compare("true",naf, true) == 0) {
                mNafNodes.Add(new Rectangle(tmpNode.x, tmpNode.y, tmpNode.width, tmpNode.height));
              }

            }

            if (nodeCreated) {
              if (mRootNode == null)
              {
                mRootNode = this.mWorkingNode;
              }
              if (this.mParentNode != null)
                this.mParentNode.addChild(this.mWorkingNode);
            }
          }

          public void endElement(String uri, String localName, String qName)
          {
            if (this.mParentNode != null)
            {
              this.mWorkingNode = this.mParentNode;
              this.mParentNode = this.mParentNode.getParent();
            }
          }

          private List<AttributePair> getAtrributes(XmlReader reader)
          {
              List<AttributePair> ret = new List<AttributePair>();
              if (reader.HasAttributes)
              {
                  Console.WriteLine("Attributes of <" + reader.Name + ">");
                  while (reader.MoveToNextAttribute())
                  {
                      Console.WriteLine(" {0}={1}", reader.Name, reader.Value);
                      AttributePair pair = new AttributePair(reader.Name, reader.Value);
                      ret.Add(pair);
                  }
                  // Move the reader back to the element node.
                  reader.MoveToElement();
              }

              return ret;
          }

          public BasicTreeNode ParserXml(Stream stream) 
          {
               mRootNode = null;
               mNafNodes = new List<Rectangle>();
               XmlReaderSettings settings = new XmlReaderSettings();

               using (XmlReader reader = XmlReader.Create(stream, settings)) 
               {
                  while(reader.Read()) 
                  {
                     switch (reader.NodeType) 
                     {
                     case XmlNodeType.Element:
                        Console.WriteLine("Start Element {0}", reader.Name);
                        startElement(reader.BaseURI, reader.LocalName, reader.Name, getAtrributes(reader));
                        if (reader.IsEmptyElement)
                        {
                            endElement(reader.BaseURI, reader.LocalName, reader.Name);
                        }
                        break;
                     case XmlNodeType.Text:
                        break;
                     case XmlNodeType.EndElement:
                          Console.WriteLine("End Element {0}", reader.Name);
                          endElement(reader.BaseURI, reader.LocalName, reader.Name);
                          break;
                     default:
                          Console.WriteLine("Other node {0} with value {1}", 
                                          reader.NodeType, reader.Value);
                          break;
                     }
                  }
               }
               return mRootNode;
        }

//      public BasicTreeNode parseXml(String xmlPath)
//      {
//        this.mRootNode = null;
//        this.mNafNodes = new List<Rectangle>();

//        SAXParserFactory factory = SAXParserFactory.newInstance();
//        SAXParser parser = null;
//        try {
//          parser = factory.newSAXParser();
//        } catch (ParserConfigurationException e) {
//          e.printStackTrace();
//          return null;
//        } catch (SAXException e) {
//          e.printStackTrace();
//          return null;
//        }

//        DefaultHandler handler = new DefaultHandler() {
//          BasicTreeNode mParentNode;
//          BasicTreeNode mWorkingNode;

//          public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//            boolean nodeCreated = false;

//            this.mParentNode = this.mWorkingNode;
//            if ("hierarchy".equals(qName)) {
//              int rotation = 0;
//              for (int i = 0; i < attributes.getLength(); i++) {
//                if ("rotation".equals(attributes.getQName(i)))
//                  try {
//                    rotation = Integer.parseInt(attributes.getValue(i));
//                  }
//                  catch (NumberFormatException nfe)
//                  {
//                  }
//              }
//              this.mWorkingNode = new RootWindowNode(attributes.getValue("windowName"), rotation);
//              nodeCreated = true;
//            } else if ("node".equals(qName)) {
//              UiNode tmpNode = new UiNode();
//              for (int i = 0; i < attributes.getLength(); i++) {
//                tmpNode.addAtrribute(attributes.getQName(i), attributes.getValue(i));
//              }
//              this.mWorkingNode = tmpNode;
//              nodeCreated = true;

//              String naf = tmpNode.getAttribute("NAF");
//              if ("true".equals(naf)) {
//                UiHierarchyXmlLoader.this.mNafNodes.add(new Rectangle(tmpNode.x, tmpNode.y, tmpNode.width, tmpNode.height));
//              }

//            }

//            if (nodeCreated) {
//              if (UiHierarchyXmlLoader.this.mRootNode == null)
//              {
//                UiHierarchyXmlLoader.this.mRootNode = this.mWorkingNode;
//              }
//              if (this.mParentNode != null)
//                this.mParentNode.addChild(this.mWorkingNode);
//            }
//          }

//          public void endElement(String uri, String localName, String qName)
//            throws SAXException
//          {
//            if (this.mParentNode != null)
//            {
//              this.mWorkingNode = this.mParentNode;
//              this.mParentNode = this.mParentNode.getParent();
//            }
//          }
        
//public  object[] await { get; set; }};
//        try {
//          parser.parse(new File(xmlPath), handler);
//        } catch (SAXException e) {
//          e.printStackTrace();
//          return null;
//        } catch (IOException e) {
//          e.printStackTrace();
//          return null;
//        }
//        return this.mRootNode;
//      }

      public List<Rectangle> getNafNodes()
      {
        return this.mNafNodes;
      }
    }
}
