﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AndroiduiDump.Tree
{
    class UiNode : BasicTreeNode
    {
        private static Regex rx = new Regex(@"\[-?(\d+),-?(\d+)\]\[-?(\d+),-?(\d+)\]");

        private Dictionary<String, String> mAttributes = new Dictionary<String, String>();
        private String mDisplayName = "ShouldNotSeeMe";
        private Object[] mCachedAttributesArray;

        public void addAtrribute(String key, String value)
        {
            this.mAttributes.Add(key, value);
            updateDisplayName();
            if (string.Compare("bounds",key, true) == 0)
                updateBounds(value);
        }

        public Dictionary<String, String> getAttributes()
        {
            return this.mAttributes;
        }

        private string GetAttributesValue(Dictionary<string,string> attribs,string sKey)
        {
            return attribs.ContainsKey(sKey) ? (String)this.mAttributes[sKey] : null;
        }

        private void updateDisplayName()
        {
            String className = GetAttributesValue(mAttributes, "class");
            if (className == null)
                return;
            String text = GetAttributesValue(mAttributes, "text"); 
            if (text == null)
                return;
            String contentDescription = GetAttributesValue(mAttributes, "content-desc");
            if (contentDescription == null)
                return;
            String index = GetAttributesValue(mAttributes, "index");
            if (index == null)
                return;
            String bounds = GetAttributesValue(mAttributes, "bounds");
            if (bounds == null)
            {
                return;
            }

            className = className.Replace("android.widget.", "");
            className = className.Replace("android.view.", "");
            StringBuilder builder = new StringBuilder();
            builder.Append('(');
            builder.Append(index);
            builder.Append(") ");
            builder.Append(className);
            if (!string.IsNullOrEmpty(text))
            {
                builder.Append(':');
                builder.Append(text);
            }
            if (!string.IsNullOrEmpty(contentDescription))
            {
                builder.Append(" {");
                builder.Append(contentDescription);
                builder.Append('}');
            }
            builder.Append(' ');
            builder.Append(bounds);
            this.mDisplayName = builder.ToString();
        }

        private void updateBounds(String bounds)
        {
            MatchCollection matches = rx.Matches(bounds);
            foreach (Match match in matches)
            {
                GroupCollection gc = match.Groups;
                if (gc.Count > 4)
                {
                    this.x = Convert.ToInt32(gc[1].Value);
                    this.y = Convert.ToInt32(gc[2].Value);
                    this.width = (Convert.ToInt32(gc[3].Value) - this.x);
                    this.height = (Convert.ToInt32(gc[4].Value) - this.y);
                    this.mHasBounds = true;
                }
                else
                {
                    throw new FormatException(new StringBuilder().Append("Invalid bounds: ").Append(bounds).ToString());
                }
            }

        }

        public String toString()
        {
            return this.mDisplayName;
        }

        public String getAttribute(String key)
        {
            return mAttributes.ContainsKey(key) ? (String)this.mAttributes[key] : null;
        }

        new public Object[] getAttributesArray()
        {
            int i;
            if (this.mCachedAttributesArray == null) {
              this.mCachedAttributesArray = new Object[this.mAttributes.Count];
              i = 0;
              foreach (String attr in this.mAttributes.Keys) {
                this.mCachedAttributesArray[(i++)] = new AttributePair(attr, (String)this.mAttributes[attr]);
              }
            }
            return this.mCachedAttributesArray;
        }

    }
}
