﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AndroiduiDump.Tree
{
    public class AttributePair
    {
        public String key;
        public String value;

        public AttributePair(String key, String value)
        {
            this.key = key;
            this.value = value;
        }
    }
}
