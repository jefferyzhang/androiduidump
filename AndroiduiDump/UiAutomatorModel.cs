﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AndroiduiDump.Tree;
using System.Drawing;
using System.IO;

namespace AndroiduiDump
{
    class UiAutomatorModel
    {
      private BasicTreeNode mRootNode;
      private BasicTreeNode mSelectedNode;
      private Rectangle mCurrentDrawingRect;
      private List<Rectangle> mNafNodes;
      private Boolean mExploreMode = true;

      private Boolean mShowNafNodes = false;

      public UiAutomatorModel(string xmlDumpFile) {
        UiHierarchyXmlLoader loader = new UiHierarchyXmlLoader();
        FileStream fs = File.OpenRead(xmlDumpFile);
        BasicTreeNode rootNode = loader.ParserXml(fs);
        if (rootNode == null) {
          throw new ArgumentException("Invalid ui automator hierarchy file.");
        }

        this.mNafNodes = loader.getNafNodes();
        if (this.mRootNode != null) {
          this.mRootNode.clearAllChildren();
        }

        this.mRootNode = rootNode;
        this.mExploreMode = true;
      }

      public BasicTreeNode getXmlRootNode() {
        return this.mRootNode;
      }

      public BasicTreeNode getSelectedNode() {
        return this.mSelectedNode;
      }

      public void setSelectedNode(BasicTreeNode node)
      {
        this.mSelectedNode = node;
        if ((this.mSelectedNode is UiNode)) {
          UiNode uiNode = (UiNode)this.mSelectedNode;
          this.mCurrentDrawingRect = new Rectangle(uiNode.x, uiNode.y, uiNode.width, uiNode.height);
        } else {
          this.mCurrentDrawingRect = Rectangle.Empty;
        }
      }

      public Rectangle getCurrentDrawingRect() {
        return this.mCurrentDrawingRect;
      }

      public BasicTreeNode updateSelectionForCoordinates(int x, int y)
      {
        BasicTreeNode node = null;

        if (this.mRootNode != null) 
        {
          MinAreaFindNodeListener listener = new MinAreaFindNodeListener(null);
          Boolean found = this.mRootNode.findLeafMostNodesAtPoint(x, y, listener);
          if ((found) && (listener.mNode != null) && (listener.mNode!=this.mSelectedNode)) {
            node = listener.mNode;
          }
        }

        return node;
      }

      public Boolean isExploreMode() {
        return this.mExploreMode;
      }

      public void toggleExploreMode() {
        this.mExploreMode = (!this.mExploreMode);
      }

      public void setExploreMode(Boolean exploreMode) {
        this.mExploreMode = exploreMode;
      }

      public List<Rectangle> getNafNodes()
      {
        return this.mNafNodes;
      }

      public void toggleShowNaf() {
        this.mShowNafNodes = (!this.mShowNafNodes);
      }

      public Boolean shouldShowNafNodes() {
        return this.mShowNafNodes;
      }

      public class MinAreaFindNodeListener:
         BasicTreeNode.IFindNodeListener
      {
        public BasicTreeNode mNode = null;

        public MinAreaFindNodeListener(BasicTreeNode Node)
        {
            mNode = Node;
        }

        void BasicTreeNode.IFindNodeListener.onFoundNode(BasicTreeNode node)
        {
          if (this.mNode == null) {
            this.mNode = node;
          }
          else if (node.height * node.width < this.mNode.height * this.mNode.width)
            this.mNode = node;
        }
      }
    }
}
