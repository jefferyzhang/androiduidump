﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using AndroiduiDump.Tree;

namespace AndroiduiDump
{
    public partial class frmMain : Form
    {
        private UiAutomatorModel uiloader;
        static private Rectangle selRect = Rectangle.Empty;
        public frmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripMenuItem2.Click += new System.EventHandler(this.menuStrip1_ItemClicked);
        }
        private void AddItemToTree(TreeNode ParentNode, UiNode bTreeNode)
        {
            if (ParentNode == null) return;
            TreeNode node = new TreeNode(bTreeNode.toString());
            node.Tag = bTreeNode;
            ParentNode.Nodes.Add(node);
            foreach (UiNode nn in bTreeNode.getChildrenList())
            {
                AddItemToTree(node, nn);
            }
        }
        private void menuStrip1_ItemClicked(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    UiAutomatorModel uiloader = new UiAutomatorModel(file);
                    TreeNode ParentNode = treeView1.Nodes.Add(uiloader.getXmlRootNode().ToString());
                    foreach (UiNode aa in uiloader.getXmlRootNode().getChildren())
                    {
                        AddItemToTree(ParentNode, aa);
                    }
                 }
                catch (Exception)
                {
                }
            }
        }
        #region Zooming Methods
        static private int ZOOMFACTOR = 2;	// = 25% smaller or larger
        /// <summary>
        /// Make the PictureBox dimensions larger to effect the Zoom.
        /// </summary>
        /// <remarks>Maximum 5 times bigger</remarks>
        private void ZoomIn()
        {
            pictureBox1.Width = Convert.ToInt32(pictureBox1.Width * ZOOMFACTOR);
            pictureBox1.Height = Convert.ToInt32(pictureBox1.Height * ZOOMFACTOR);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        /// <summary>
        /// Make the PictureBox dimensions smaller to effect the Zoom.
        /// </summary>
        /// <remarks>Minimum 5 times smaller</remarks>
        private void ZoomOut()
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Width = Convert.ToInt32(pictureBox1.Width / ZOOMFACTOR);
            pictureBox1.Height = Convert.ToInt32(pictureBox1.Height / ZOOMFACTOR);
        }

        #endregion
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            string[] comlist = {"rm /data/local/tmp/uidump.xml",
                                "rm /data/local/tmp/uidump.xml",
                                "/system/bin/uiautomator dump /data/local/tmp/uidump.xml",
                                "/system/bin/screencap -p /data/local/tmp/uidump.png"
                                };
            adbHelper helper = new adbHelper(@"D:\Android Dev\sdk\platform-tools\adb.exe");
            helper.runAdb("53098953", comlist);

            string stempPNG = Path.Combine(System.Environment.GetEnvironmentVariable("TEMP"),"uidump.png");
            string stempxml = Path.Combine(System.Environment.GetEnvironmentVariable("TEMP"),"uidump.xml");
            helper.runAdb(string.Format("pull /data/local/tmp/uidump.png {0}",stempPNG));
            helper.runAdb(string.Format("pull /data/local/tmp/uidump.xml {0}", stempxml));

            if (File.Exists(stempPNG))
            {
                Image img = Image.FromFile(stempPNG);
                pictureBox1.Width = img.Width;
                pictureBox1.Height = img.Height;
                pictureBox1.Image = img;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                ZoomOut();
            }

            if (File.Exists(stempxml))
            {
                try
                {
                    uiloader = new UiAutomatorModel(stempxml);
                    TreeNode ParentNode = treeView1.Nodes.Add(uiloader.getXmlRootNode().ToString());
                    foreach (UiNode aa in uiloader.getXmlRootNode().getChildren())
                    {
                        AddItemToTree(ParentNode, aa);
                    }
                }
                catch (Exception)
                {
                }
            }

        }

        private void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // ZoomIn();
        }

        private void zoomOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ZoomOut();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (selRect != Rectangle.Empty)
            {
                Pen pen = new Pen(Color.Blue);
                e.Graphics.DrawRectangle(pen, selRect);
            }
        }

 

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            toolStripStatusLabel1.Text = string.Format("Pos X={0} Y={1}", e.X * ZOOMFACTOR, e.Y * ZOOMFACTOR);
            BasicTreeNode node = uiloader.updateSelectionForCoordinates(e.X * ZOOMFACTOR, e.Y * ZOOMFACTOR);
            selRect = new Rectangle(node.x / ZOOMFACTOR, node.y / ZOOMFACTOR, node.width / ZOOMFACTOR, node.height / ZOOMFACTOR);
            TreeNode tnNode = null;
            foreach (TreeNode tn in treeView1.Nodes)
            {
                tnNode = FindNode(tn, node);
                if (tn != null) break;
            }
            if (tnNode != null)
            {
                tnNode.Parent.ExpandAll();
                treeView1.SelectedNode = tnNode;
                treeView1.Focus();
            }
            pictureBox1.Invalidate();
        }

        private TreeNode FindNode(TreeNode tnParent, BasicTreeNode node)
        {
            TreeNode tnRet = null;

            if (tnParent == null) return null;
            if (tnParent.Tag == node) return tnParent;

            foreach (TreeNode tn in tnParent.Nodes)
            {
                tnRet = FindNode(tn, node);
                if (tnRet != null) break;
            }

            return tnRet;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                BasicTreeNode node = (BasicTreeNode)e.Node.Tag;
                selRect = new Rectangle(node.x / ZOOMFACTOR, node.y / ZOOMFACTOR, node.width / ZOOMFACTOR, node.height / ZOOMFACTOR);
                pictureBox1.Invalidate();
            }
        }

        private void splitContainer1_Panel1_Resize(object sender, EventArgs e)
        {
            pictureBox1.Left = (splitContainer1.Panel1.Width -  pictureBox1.Width) / 2;
            pictureBox1.Top = (splitContainer1.Panel1.Height - pictureBox1.Height) / 2;
        }
    }
}
